.PHONY: install-user-original install-user-plasma install-system-original install-system-plasma uninstall-user-original uninstall-user-plasma uninstall-system-original uninstall-system-plasma

install-user-original:
	sh ./scripts/install-user-original.sh

install-user-plasma:
	sh ./scripts/install-user-plasma.sh

install-system-original:
	sh ./scripts/install-system-original.sh

install-system-plasma:
	sh ./scripts/install-system-plasma.sh

uninstall-user-original:
	sh ./scripts/uninstall-user-original.sh

uninstall-user-plasma:
	sh ./scripts/uninstall-user-plasma.sh

uninstall-system-original:
	sh ./scripts/uninstall-system-original.sh

uninstall-system-plasma:
	sh ./scripts/uninstall-system-plasma.sh
