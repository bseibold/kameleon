# Kameleon (Look and Feel)

This theme is part of the Kameleon project

## Project

Kameleon is a project to create a full desktop theming to (I know, I know) looking like Windows 10. Nowadays Windows 10 seems to looking like KDE so ...

## Configuration

Support of virtual keyboard is functionnal (top right of the screen).

If you don't have virtual keyboard showing up (Debian Buster for exemple), just install it :

```
sudo apt install virtualkeyboard-plugin
```

## Tweaks

### Background

Open theme folder then go to :

* ./contents/components/artwork/

Replace the "background.png" file to the one you like (it needs to be .png file) 

### Avatars

Open theme folder then go to :

* ./contents/components/artwork/

Replace the "user.png" file to the one you like (it needs to be .png file)

### Timers

Open theme folder then edit the file :

* ./contents/lockscreen/LockScreenUi.qml

```
Line 86 :

interval: 30000 // set the interval you want the cover to appear default is 30 seconds
```

### Colors

Open theme folder then edit all of this file :

* ./contents/lockscreen/LockScreenUi.qml

* ./contents/lockscreen/MainBlock.qml

* ./contents/logout/Logout.qml

* ./contents/splash/Splash.qml

```
In each file, look for :

color: "#1e92ff"

Replace the part "#1e92ff" to the one you want, this will apply your color to all component of the theme if you edit all the file (go for a search and replace)
```

### Language

You can change the language text part to feet your language, in order to do this, you have to change the following part in differents files :

#### Splash

To change the "Welcome" text in the splash screen :

Edit ./splash/Splash.qml

```
Line 113 : 

text: i18nd("plasma_lookandfeel_org.kde.lookandfeel","Welcome")
```

#### Logout

You can change the logout message saying for exemple "Reboot in X seconds" to "Rebooting" by modifying the following lines :

Edit ./logout/Logout.qml

```
Line 185 :

Uncomment the line and add for example (for French language) "Redémarrage en cours" instead of "Your language here"

return i18ndp("plasma_lookandfeel_org.kde.lookandfeel", "Rebooting", "Your language here");

Line 190 :

Uncomment the line and add for example (for French language) "Arrêt en cours" instead of "Your language here"

// return i18ndp("plasma_lookandfeel_org.kde.lookandfeel", "Shutting down", "Your language here");

Line 195 :

Uncomment the line and add for example (for French language) "Déconnexion" instead of "Your language here"

// return i18ndp("plasma_lookandfeel_org.kde.lookandfeel", "Logging out", "Your language here");

Comment the line 187, 192 and 197 with "//" to disable original plasma messages
```

## Credits

This theme is adapt from original Breeze. All copyrights, credits and everything goes to their original authors.

## Notes

Since I don't understand well QML language, I've trying to do what I could to obtain the things I want. Feel free to modify to feet your needs, you can also submit part of code that I could add to the theme when I find some time.

I'm using this theme daily and so far got no issue, I wanted to share with community since a long time, enjoy if you like.

Feel free to copy the theme and made your own change !


