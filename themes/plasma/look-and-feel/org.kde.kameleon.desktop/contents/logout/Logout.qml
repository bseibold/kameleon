/*  
*   Modification made by Robot WML for Kameleon 
*/

/***************************************************************************
 *   Copyright (C) 2014 by Aleix Pol Gonzalez <aleixpol@blue-systems.com>  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .        *
 ***************************************************************************/

import QtQuick 2.2
import QtQuick.Layouts 1.2
import QtQuick.Controls 1.1 as Controls

import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.components 2.0 as PlasmaComponents
import org.kde.kcoreaddons 1.0 as KCoreAddons

import "../components"
import "timer.js" as AutoTriggerTimer

import org.kde.plasma.private.sessions 2.0

PlasmaCore.ColorScope {
    id: root
    colorGroup: PlasmaCore.Theme.ComplementaryColorGroup
    height: screenGeometry.height
    width: screenGeometry.width

    signal logoutRequested()
    signal haltRequested()
    signal suspendRequested(int spdMethod)
    signal rebootRequested()
    signal rebootRequested2(int opt)
    signal cancelRequested()
    signal lockScreenRequested()

    property alias backgroundColor: backgroundRect.color

    function sleepRequested() {
        root.suspendRequested(2);
    }

    function hibernateRequested() {
        root.suspendRequested(4);
    }

    // you can set the timeout here in seconds, modify the original 30 to 5
    property real timeout: 5
    property real remainingTime: root.timeout
    property var currentAction: {
        switch (sdtype) {
            case ShutdownType.ShutdownTypeReboot:
                return root.rebootRequested;
            case ShutdownType.ShutdownTypeHalt:
                return root.haltRequested;
            default:
                return root.logoutRequested;
        }
    }

    property int stage

    onStageChanged: {
        if (stage == 1) {
            introAnimation.running = true;
        } else if (stage == 2) {
            introAnimation.target = spinner;
            introAnimation.from = 1;
            introAnimation.to = 0;
            introAnimation.running = true;
        }
    }

    KCoreAddons.KUser {
        id: kuser
    }
    
    Controls.Action {
        onTriggered: root.cancelRequested()
        shortcut: "Escape"
    }

    onRemainingTimeChanged: {
        if (remainingTime <= 0) {
            root.currentAction();
        }
    }

    Timer {
        id: countDownTimer
        running: true
        repeat: true
        interval: 1000
        onTriggered: remainingTime--
        Component.onCompleted: {
            AutoTriggerTimer.addCancelAutoTriggerCallback(function() {
                countDownTimer.running = false;
            });
        }
    }

    function isLightColor(color) {
        return Math.max(color.r, color.g, color.b) > 0.5
    }

    Rectangle {
        id: backgroundRect
        anchors.fill: parent
        color: "#1e92ff"
    }

    MouseArea {
        anchors.fill: parent
        onClicked: root.cancelRequested()
    }

    ColumnLayout {
        anchors {
            top: parent.verticalCenter
            horizontalCenter: parent.horizontalCenter
        }

        Image {
            id: spinner
            //again sync from SDDM theme
            anchors {
                bottom: parent.top
                horizontalCenter: parent.horizontalCenter
            }
            height: 35
            width: 35
            smooth: true
            source: "../components/artwork/spinner.svgz"
            RotationAnimator on rotation {
                id: rotationAnimator
                running: true
                from: 0
                to: 360
                duration: 1500
                loops: Animation.Infinite
            }
        }

        PlasmaComponents.Label {
            Layout.alignment: Qt.AlignHCenter
            //opacity, as visible would re-layout
            opacity: countDownTimer.running ? 1 : 0
            anchors {
                top: spinner.bottom
                topMargin: 20
            }
            Behavior on opacity {
                OpacityAnimator {
                    duration: units.longDuration
                    easing.type: Easing.InOutQuad
                }
            }
            text: {
                switch (sdtype) {
                    case ShutdownType.ShutdownTypeReboot:
                        // Change here the text "Your language here" if you want to add the "Rebooting" text to your language
                        // return i18ndp("plasma_lookandfeel_org.kde.lookandfeel", "Rebooting", "Your language here");
                        // If you want the original text from Breeze, uncomment this next return and comment previous text
                        return i18ndp("plasma_lookandfeel_org.kde.lookandfeel", "Reboot in 1 second", "Reboot in %1 seconds", root.remainingTime);
                    case ShutdownType.ShutdownTypeHalt:
                        // Change here the text "Your language here" if you want to add the "Shutting down" text to your language
                        // return i18ndp("plasma_lookandfeel_org.kde.lookandfeel", "Shutting down", "Your language here");
                        // If you want the original text from Breeze, uncomment this next return and comment previous text
                        return i18ndp("plasma_lookandfeel_org.kde.lookandfeel", "Shutting down in 1 second", "Shutting down in %1 seconds", root.remainingTime);
                    default:
                        // Change here the text "Your language here" if you want to add the "Logging out" text to your language
                        // return i18ndp("plasma_lookandfeel_org.kde.lookandfeel", "Logging out", "Your language here");
                        // If you want the original text from Breeze, uncomment this next return and comment previous text
                        return i18ndp("plasma_lookandfeel_org.kde.lookandfeel", "Logging out in 1 second", "Logging out in %1 seconds", root.remainingTime);
                }
            }
            font.pointSize: 18
            color: "white"
        }

    }

}
