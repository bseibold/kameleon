/*  
*   Modification made by Robot WML for Kameleon 
*/

/*
 *   Copyright 2016 David Edmundson <davidedmundson@kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2 or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import QtQuick 2.2

import QtQuick.Layouts 1.1
import QtQuick.Controls 1.1

import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.components 2.0 as PlasmaComponents

import "../components"

SessionManagementScreen {
    /*
     * Login has been requested with the following username and password
     * If username field is visible, it will be taken from that, otherwise from the "name" property of the currentIndex
     */
    signal loginRequest(string password)

    /*
     */
    signal newSession()

    function startLogin() {
        if (userListCurrentIndex == 1) {
            newSession()
            return;
        }

        var password = passwordBox.text

        //this is partly because it looks nicer
        //but more importantly it works round a Qt bug that can trigger if the app is closed with a TextField focussed
        //See https://bugreports.qt.io/browse/QTBUG-55460
        loginButton.forceActiveFocus();
        loginRequest(password);
    }

    Rectangle {
        id: userNameBlock
        Layout.fillWidth: true
        Layout.minimumHeight: 70
        Layout.maximumHeight: 70
        Layout.topMargin: -80
        Layout.rightMargin:-10
        Layout.leftMargin:-10
        color: "transparent"

        Text {
            anchors {
                left: parent.left
                right: parent.right
                top: parent.top
            }
            text: userList.selectedUser
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.pointSize: 28
            color: "white"
        }

    }
    
    PlasmaComponents.TextField {
        id: passwordBox
        Layout.fillWidth: true
        Layout.minimumHeight: 40
        Layout.maximumHeight: 40
        Layout.topMargin: 5
        Layout.rightMargin: 26
        Layout.leftMargin:-10

        placeholderText: i18nd("plasma_lookandfeel_org.kde.lookandfeel", "Password")
        focus: true
        echoMode: TextInput.Password
        revealPasswordButtonShown: true
        enabled: !authenticator.graceLocked

        onAccepted: startLogin()

        visible: userListCurrentIndex == 0

        //if empty and left or right is pressed change selection in user switch
        //this cannot be in keys.onLeftPressed as then it doesn't reach the password box
        Keys.onPressed: {
            coverTimer.restart()
            if (event.key == Qt.Key_Left && !text) {
                userList.decrementCurrentIndex();
                event.accepted = true
            }
            if (event.key == Qt.Key_Right && !text) {
                userList.incrementCurrentIndex();
                event.accepted = true
            }
        }

        Connections {
            target: root
            onClearPassword: {
                passwordBox.forceActiveFocus()
                passwordBox.selectAll()
            }
        }
    }

    Rectangle {
        id: loginButton
        state: userListCurrentIndex == 0 ? "visible" : "hidden"
        Layout.minimumWidth: 40
        Layout.maximumWidth: 40
        Layout.minimumHeight: 40
        Layout.maximumHeight: 40
        anchors {
            right: passwordBox.right
            rightMargin: -37
            verticalCenter: passwordBox.verticalCenter
        }
        color: "#1e92ff"
        radius: 2

        function showHide() {
            state = state == "hidden" ? "visible" : "hidden";
        }

        Image {
            height: 36
            width: 36
            anchors {
                top: loginButton.top
                right: loginButton.right
                verticalCenter: parent.verticalCenter
            }
            source: "../components/artwork/login_primary.svgz"
            smooth: true
            MouseArea {
                anchors.fill: parent
                onClicked: startLogin();
            }
        }

        states: [
            State {
                name: "visible"
                PropertyChanges {
                    target: loginButton
                    opacity: 1
                }
            },
            State {
                name: "hidden"
                PropertyChanges {
                    target: loginButton
                    opacity: 0
                }
            }
        ]
    }

    Rectangle {
        id: newSessionText
        state: userListCurrentIndex == 0 ? "hidden" : "visible"
        Layout.fillWidth: true
        Layout.minimumHeight: 60
        Layout.maximumHeight: 60
        Layout.topMargin:-195
        Layout.rightMargin:-50
        Layout.leftMargin:-50
        color: "transparent"

        function showHide() {
            state = state == "hidden" ? "visible" : "hidden";
        }

        Text {
            anchors {
                left: parent.left
                right: parent.right
                top: parent.top
            }
            text: i18nd("plasma_lookandfeel_org.kde.lookandfeel","Different User")
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.pointSize: 28
            color: "white"
        }

        states: [
            State {
                name: "visible"
                PropertyChanges {
                    target: newSessionText
                    opacity: 1
                }
            },
            State {
                name: "hidden"
                PropertyChanges {
                    target: newSessionText
                    opacity: 0
                }
            }
        ]

    }

    Rectangle {
        id: newSessionButton
        state: userListCurrentIndex == 0 ? "hidden" : "visible"
        Layout.minimumHeight: 40
        Layout.maximumHeight: 40
        Layout.minimumWidth: 300
        Layout.maximumWidth: 300
        Layout.topMargin:-60
        Layout.rightMargin:-50
        Layout.leftMargin:-50
        anchors {
            horizontalCenter: parent.horizontalCenter
        }
        color: "#1e92ff"
        radius: 2

        function showHide() {
            state = state == "hidden" ? "visible" : "hidden";
        }

        Text {
            anchors {
                left: parent.left
                right: parent.right
                top: parent.top
                bottom: parent.bottom
                verticalCenter: parent.verticalCenter
            }
            text: i18nd("plasma_lookandfeel_org.kde.lookandfeel", "Start New Session")
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            color: "white"
        }

        MouseArea {
            anchors.fill: parent
            onClicked: startLogin();
        }

        states: [
            State {
                name: "visible"
                PropertyChanges {
                    target: newSessionButton
                    opacity: 1
                }
            },
            State {
                name: "hidden"
                PropertyChanges {
                    target: newSessionButton
                    opacity: 0
                }
            }
        ]

    }
}
