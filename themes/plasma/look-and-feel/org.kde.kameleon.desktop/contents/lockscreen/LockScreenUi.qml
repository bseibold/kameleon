/*  
*   Modification made by Robot WML for Kameleon 
*/

/********************************************************************
 This file is part of the KDE project.

Copyright (C) 2014 Aleix Pol Gonzalez <aleixpol@blue-systems.com>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

import QtQuick 2.5
import QtQuick.Controls 1.1
import QtQuick.Layouts 1.1

import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.components 2.0 as PlasmaComponents
import org.kde.plasma.extras 2.0 as PlasmaExtras

import org.kde.plasma.private.sessions 2.0
import "../components"

PlasmaCore.ColorScope {
    id: lockScreenRoot
    
    Rectangle {
        anchors {
            fill: parent
        }
        color: "#1e92ff"
    }
    
    Image {
        anchors {
            fill: parent
        }
        clip: true
        focus: true
        smooth: true
        source: "../components/artwork/background.png"
    }
    
    Rectangle {
        anchors {
            fill: parent
        }
        color: "#000000"
        opacity: 0.3
    }
    
    colorGroup: PlasmaCore.Theme.ComplementaryColorGroup

    Connections {
        target: authenticator
        onFailed: {
            root.notification = i18nd("plasma_lookandfeel_org.kde.lookandfeel","Unlocking failed");
        }
        onGraceLockedChanged: {
            if (!authenticator.graceLocked) {
                root.notification = "";
                root.clearPassword();
            }
        }
        onMessage: {
            root.notification = msg;
        }
        onError: {
            root.notification = err;
        }
    }

    Timer {
        id: coverTimer
        interval: 30000 // set the interval you want the cover to appear default is 30 seconds
        onTriggered: {
            if (cover.state == "hidden") {
                cover.state = "visible"
                cover.forceActiveFocus()
            }
        }
    }

    // hide the cover when a key is pressed
    Keys.onPressed: {
        coverTimer.restart()
        if (cover.state == "visible") {
            cover.state = "hidden"
            mainStack.forceActiveFocus()
        }
    }

    SessionsModel {
        id: sessionsModel
        showNewSessionEntry: true
    }

    PlasmaCore.DataSource {
        id: keystateSource
        engine: "keystate"
        connectedSources: "Caps Lock"
    }

    Loader {
        id: changeSessionComponent
        active: false
        source: "ChangeSession.qml"
        visible: false
    }

    Image {
        id: logo
        anchors {
            bottomMargin: 55
            bottom: parent.verticalCenter
            horizontalCenter: parent.horizontalCenter
        }
        source: "../components/artwork/user.png"
        width: 200
        height: 200
    }
    
    PlasmaComponents.ToolButton {
        id: keyboardIcon
        state: "hidden"
        function showHide() {
            state = state == "hidden" ? "visible" : "hidden";
        }
        anchors {
            top: parent.top
            right: parent.right
        }
        height: 60
        width: 60
        iconName: inputPanel.keyboardActive ? "input-keyboard-virtual-on" : "input-keyboard-virtual-off"
        onClicked: { 
            inputPanel.showHide() 
            keyboardBackground.showHide()
            keyboardIcon.showHide()
        }
    }

    ListModel {
        id: users

        Component.onCompleted: {
            users.append({name: kscreenlocker_userName,
                            realName: kscreenlocker_userName,
                            // uncomment the line bellow and comment the other "icon" to get the original screen locker logo for each session
                            // icon: kscreenlocker_userImage,
                            icon: "../components/artwork/user.png",

            })
            if (sessionsModel.canStartNewSession) {
                users.append({realName: i18nd("plasma_lookandfeel_org.kde.lookandfeel", ""),
                                name: "",
                                iconName: "list-add"
                })
            }
        }
    }

    StackView {
        id: mainStack
        anchors {
            fill: parent
        }
        focus: true //StackView is an implicit focus scope, so we need to give this focus so the item inside will have it
        initialItem: MainBlock {
            userListModel: users

            onNewSession: {
                sessionsModel.startNewSession(false);
            }

            onLoginRequest: {
                root.notification = ""
                authenticator.tryUnlock(password)
            }

            notificationMessage: {
                var text = ""
                if (keystateSource.data["Caps Lock"]["Locked"]) {
                    text += i18nd("plasma_lookandfeel_org.kde.lookandfeel","Caps Lock is on")
                    if (root.notification) {
                        text += " • "
                    }
                }
                text += root.notification
                return text
            }

            actionItems: [
                KeyboardLayoutButton {
                    
                },
                Battery {
                    
                },
                ActionButton {
                    iconSource: "system-lock-screen-symbolic"
                },
                ActionButton {
                    iconSource: "system-switch-user"
                    onClicked: mainStack.push(switchSessionPage)
                    visible: sessionsModel.count > 1 && sessionsModel.canSwitchUser
                }
            ]
            
            Loader {
                Layout.fillWidth: true
                Layout.preferredHeight: item ? item.implicitHeight : 0
                active: true // TODO configurable
                source: "MediaControls.qml"
            }
        }
    }

    Component {
        id: switchSessionPage
        SessionManagementScreen {
            userListModel: sessionsModel

            Rectangle {
                id: switchSessionText
                Layout.fillWidth: true
                Layout.minimumHeight: 60
                Layout.maximumHeight: 60
                Layout.topMargin:-95
                Layout.rightMargin:-50
                Layout.leftMargin:-50
                color: "transparent"

                Text {
                    anchors {
                        left: parent.left
                        right: parent.right
                        top: parent.top
                    }
                    text: i18nd("plasma_lookandfeel_org.kde.lookandfeel","Different User")
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    font.pointSize: 28
                    color: "white"
                }

            }

            Rectangle {
                id: switchSessionButton
                Layout.minimumHeight: 40
                Layout.maximumHeight: 40
                Layout.minimumWidth: 300
                Layout.maximumWidth: 300
                Layout.topMargin: 0
                Layout.rightMargin:-50
                Layout.leftMargin:-50
                anchors {
                    horizontalCenter: parent.horizontalCenter
                }
                color: "#1e92ff"
                radius: 2

                Text {
                    anchors {
                        left: parent.left
                        right: parent.right
                        top: parent.top
                        bottom: parent.bottom
                        verticalCenter: parent.verticalCenter
                    }
                    text: i18nd("plasma_lookandfeel_org.kde.lookandfeel", "Switch Session")
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    color: "white"
                }

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        sessionsModel.switchUser(userListCurrentModelData.vtNumber)
                        mainStack.pop()
                    }
                }

                states: [
                    State {
                        name: "visible"
                        PropertyChanges {
                            target: newSessionButton
                            opacity: 1
                        }
                    },
                    State {
                        name: "hidden"
                        PropertyChanges {
                            target: newSessionButton
                            opacity: 0
                        }
                    }
                ]
            }

            actionItems: [
                KeyboardLayoutButton {
                    
                },
                Battery {
                    
                },
                ActionButton {
                    iconSource: "system-users"
                    onClicked: mainStack.pop()
                }
            ]

        }

    }
    
    Rectangle {
        id: keyboardBackground
        state: "hidden"
        anchors {
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }
        height: 470
        color: "transparent"

        function showHide() {
            state = state == "hidden" ? "visible" : "hidden";
        }
            
        states: [
            State {
                name: "visible"
                PropertyChanges {
                    target: keyboardBackground
                    opacity: 1
                }
            },
            State {
                name: "hidden"
                PropertyChanges {
                    target: keyboardBackground
                    opacity: 0
                }
            }
        ]
    
        Loader {
            id: inputPanel
            state: "hidden"
            readonly property bool keyboardActive: item ? item.active : false
            anchors {
                left: parent.left
                leftMargin: 250
                right: parent.right
                rightMargin: 250
                bottom: parent.bottom
            }

            function showHide() {
                state = state == "hidden" ? "visible" : "hidden";
            }

            Component.onCompleted: inputPanel.source = "../components/VirtualKeyboard.qml"
            
            states: [
                State {
                    name: "visible"
                    PropertyChanges {
                        target: inputPanel
                        opacity: 1
                    }
                },
                State {
                    name: "hidden"
                    PropertyChanges {
                        target: inputPanel
                        opacity: 0
                    }
                }
            ]

            transitions: [
                Transition {
                    from: "hidden"
                    to: "visible"
                    SequentialAnimation {
                        ScriptAction {
                            script: {
                                inputPanel.item.activated = true;
                                Qt.inputMethod.show();
                            }
                        }
                        ParallelAnimation {
                            NumberAnimation {
                                target: inputPanel
                                duration: units.longDuration
                                easing.type: Easing.OutQuad
                            }
                            OpacityAnimator {
                                target: inputPanel
                                duration: units.longDuration
                                easing.type: Easing.OutQuad
                            }
                        }
                    }
                },
                Transition {
                    from: "visible"
                    to: "hidden"
                    SequentialAnimation {
                        ParallelAnimation {
                            NumberAnimation {
                                target: inputPanel
                                duration: units.longDuration
                                easing.type: Easing.InQuad
                            }
                            OpacityAnimator {
                                target: inputPanel
                                duration: units.longDuration
                                easing.type: Easing.InQuad
                            }
                        }
                        ScriptAction {
                            script: {
                                Qt.inputMethod.hide();
                            }
                        }
                    }
                }
            ]

        }

    }

    Loader {
        anchors {
            horizontalCenter: parent.horizontalCenter
            bottom: parent.bottom
        }
        active: root.viewVisible
        source: "LockOsd.qml"
    }

    RowLayout {
        id: footer
        anchors {
            bottom: parent.bottom
            left: parent.left
            right: parent.right
            margins: units.smallSpacing
        }

        Item {
            Layout.fillWidth: true
        }
    }

    Rectangle {
        id: cover
        focus: true
        state: "visible"
        function showHide() {
            state = state == "visible" ? "visible" : "hidden";
        }
        x: 0
        y: 0
        z: 100
        width: parent.width
        height: parent.height
        
        Image {
            anchors {
                fill: parent
            }
            clip: true
            focus: true
            smooth: true
            source: "../components/artwork/background.png"

            Clock {
                anchors.bottom: parent.bottom
                anchors.bottomMargin: units.gridUnit * 6
                anchors.left: parent.left
                anchors.leftMargin: units.gridUnit * 3
            }
        }

        Rectangle {
            anchors {
                fill: parent
            }
            color: "#000000"
            opacity: 0.1
        }

        MouseArea {
            anchors.fill: parent
            onClicked: { coverTimer.restart(); cover.state = "hidden"; mainStack.forceActiveFocus() }
        }

        states: [
            State {
                name: "visible"
                PropertyChanges { target: cover; y: 0; z: 100; opacity: 1 }
            },
            State {
                name: "hidden"
                PropertyChanges { target: cover; y: - parent.height; z: 0; opacity: 0.6 }
            }
        ]

        transitions: [
            Transition {
                from: "*"; to: "hidden"
                NumberAnimation { properties: "y,opacity"; duration: 300; easing.type: Easing.OutExpo }
            },
            Transition {
                from: "*"; to: "visible"
                NumberAnimation { properties: "y,opacity"; duration: 300; easing.type: Easing.InExpo }
            }
        ]
    }

    Component.onCompleted: {
        // version support checks
        if (root.interfaceVersion < 1) {
            // ksmserver of 5.4, with greeter of 5.5
            root.viewVisible = true;
        }
    }
    
}
