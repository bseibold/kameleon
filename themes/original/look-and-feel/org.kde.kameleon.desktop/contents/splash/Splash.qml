/*  
*   Modification made by Robot WML for Kameleon 
*/

/*
 *   Copyright 2014 Marco Martin <mart@kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License version 2,
 *   or (at your option) any later version, as published by the Free
 *   Software Foundation
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import QtQuick 2.5

Rectangle {
    id: root
    color: "#1e92ff"

    Image {
        anchors {
            fill: parent
        }
        clip: true
        focus: true
        smooth: true
        source: "../components/artwork/background.png"
    }

    Rectangle {
        anchors {
            fill: parent
        }
        color: "#000000"
        opacity: 0.3
    }

    property int stage

    onStageChanged: {
        if (stage == 1) {
	    spinner.opacity = 1 
        }
        if (stage == 2) {
	    spinner.opacity = 1
        }
        if (stage == 3) {
	    spinner.opacity = 1
        }
        if (stage == 4) {
	    spinner.opacity = 1
        }
        if (stage == 5) {
	    spinner.opacity = 1
        }
        if (stage == 6) {
	    spinner.opacity = 1 
        }
    }

    Rectangle {
        id: mainBlock
        color: "transparent"
        anchors {
            verticalCenter: parent.verticalCenter
            horizontalCenter: parent.horizontalCenter
        }
        width: 300
        height: 60

        Image {
            anchors {
                bottom: parent.top
                horizontalCenter: parent.horizontalCenter
            }
            fillMode: Image.PreserveAspectFit
            source: "../components/artwork/user.png"
            width: 200
            height: 200
        }

        Image {
            id: spinner
            anchors {
                top: parent.top
                left: parent.left
                topMargin: 30
                leftMargin: 55
            }
            height: 35
            width: 35
            smooth: true
            source: "../components/artwork/spinner.png"
            NumberAnimation on opacity {
                id: animateRotation
                target: spinner
                properties: "rotation"
                from: 180
                to: 540
                duration: 1250
                loops: Animation.Infinite
                running: true
            }
        }

        Text {
            id: welcomeText
            anchors {
                top: parent.top
                topMargin: 30
                left: spinner.right
                leftMargin: 15
            }
            // Change here the text "Welcome" if you want to add the "Welcome" text to your language
            text: i18nd("plasma_lookandfeel_org.kde.lookandfeel","Welcome")
            font.pointSize: 20
            color: "white"
        }

    }

}
