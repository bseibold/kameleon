#!/bin/bash

echo "Removing aurorae theme"

rm -rf ~/.local/share/aurorae/themes/kameleon

echo "Removing color schemes theme"

rm -rf ~/.local/share/color-schemes/kameleon.colors

echo "Removing desktop theme"

rm -rf ~/.local/share/plasma/desktoptheme/kameleon-dark

rm -rf ~/.local/share/plasma/desktoptheme/kameleon-light

echo "Removing grub theme"

sudo rm -rf /boot/grub/themes/kameleon

echo "Removing gtk theme"

rm -rf ~/.local/share/themes/kameleon

echo "Removing icons theme"

rm -rf ~/.local/share/icons/kameleon

echo "Removing look and feel theme"

rm -rf ~/.local/share/plasma/look-and-feel/org.kde.kameleon.desktop

echo "Removing pixmaps icons"

rm -rf ~/.local/share/pixmaps/desktop-dark.png

rm -rf ~/.local/share/pixmaps/desktop-light.png

rm -rf ~/.local/share/pixmaps/menu-dark.png

rm -rf ~/.local/share/pixmaps/menu-light.png

rm -rf ~/.local/share/pixmaps/search-dark.png

rm -rf ~/.local/share/pixmaps/search-light.png

rm -rf ~/.local/share/pixmaps/user-circle.png

echo "Removing plymouth theme"

sudo rm -rf /usr/share/plymouth/themes/kameleon

echo "Removing qtcurve theme"

rm -rf ~/.local/share/QtCurve/kameleon.qtcurve

echo "Removing sddm theme"

sudo rm -rf /usr/share/sddm/themes/kameleon

echo "Removing wallpapers theme"

sudo rm -rf /usr/share/wallpapers/kameleon

 
 
 
 
