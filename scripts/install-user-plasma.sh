#!/bin/bash

echo "Copying aurorae theme"

cd ./themes/plasma/

mkdir -p ~/.local/share/aurorae/themes

cp -R -v ./aurorae/* ~/.local/share/aurorae/themes/

echo "Copying color schemes theme"

mkdir -p ~/.local/share/color-schemes

cp -v ./color-schemes/kameleon.colors ~/.local/share/color-schemes/kameleon.colors

echo "Copying desktop theme"

mkdir -p ~/.local/share/plasma/desktoptheme

cp -R -v ./desktoptheme/* ~/.local/share/plasma/desktoptheme/

echo "Copying grub theme"

sudo cp -R -v ./grub/* /boot/grub/themes/

sudo chmod 755 -R /boot/grub/themes/kameleon

echo "Copying gtk theme"

mkdir -p ~/.local/share/themes

cp -R -v ./gtk/* ~/.local/share/themes/

echo "Copying icons theme"

mkdir -p ~/.local/share/icons

cp -R -v ./icons/* ~/.local/share/icons/

echo "Copying look and feel theme"

mkdir -p ~/.local/share/plasma/look-and-feel

cp -R -v ./look-and-feel/* ~/.local/share/plasma/look-and-feel/

echo "Copying pixmaps icons"

mkdir -p ~/.local/share/pixmaps

cp -R -v ./pixmaps/* ~/.local/share/pixmaps/

echo "Copying qtcurve theme"

mkdir -p ~/.local/share/QtCurve

cp -v ./qtcurve/kameleon.qtcurve ~/.local/share/QtCurve/kameleon.qtcurve

echo "Copying sddm theme"

sudo cp -R -v ./sddm/* /usr/share/sddm/themes/

sudo chmod 755 -R /usr/share/sddm/themes/kameleon

echo "Copying wallpapers theme"

sudo cp -R -v ./wallpapers/* /usr/share/wallpapers/

sudo chmod 755 -R /usr/share/wallpapers/kameleon

 
 
