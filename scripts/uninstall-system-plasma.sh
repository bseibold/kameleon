#!/bin/bash

echo "Removing aurorae theme"

rm -rf /home/$USER/.local/share/aurorae/themes/kameleon

echo "Removing color schemes theme"

sudo rm -rf /usr/share/color-schemes/kameleon.colors

echo "Removing desktop theme"

sudo rm -rf /usr/share/plasma/desktoptheme/kameleon-dark

sudo rm -rf /usr/share/plasma/desktoptheme/kameleon-light

echo "Removing grub theme"

sudo rm -rf /boot/grub/themes/kameleon

echo "Removing gtk theme"

sudo rm -rf /usr/share/themes/kameleon

echo "Removing icons theme"

sudo rm -rf /usr/share/icons/kameleon

echo "Removing look and feel theme"

sudo rm -rf /usr/share/plasma/look-and-feel/org.kde.kameleon.desktop

echo "Removing pixmaps icons"

sudo rm -rf /usr/share/pixmaps/desktop-dark.png

sudo rm -rf /usr/share/pixmaps/desktop-light.png

sudo rm -rf /usr/share/pixmaps/menu-dark.png

sudo rm -rf /usr/share/pixmaps/menu-light.png

sudo rm -rf /usr/share/pixmaps/user-border.png

sudo rm -rf /usr/share/pixmaps/user-noborder.png

echo "Removing qtcurve theme"

sudo rm -rf /usr/share/QtCurve/kameleon.qtcurve

echo "Removing sddm theme"

sudo rm -rf /usr/share/sddm/themes/kameleon

echo "Removing wallpapers theme"

sudo rm -rf /usr/share/wallpapers/kameleon

 
 
