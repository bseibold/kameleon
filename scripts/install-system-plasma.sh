#!/bin/bash

echo "Copying aurorae theme"

cd ./themes/plasma/

mkdir -p /home/$USER/.local/share/aurorae/themes

cp -R -v ./aurorae/* /home/$USER/.local/share/aurorae/themes/

echo "Copying color schemes theme"

sudo cp -v ./color-schemes/kameleon.colors /usr/share/color-schemes/kameleon.colors

sudo chmod 755 /usr/share/color-schemes/kameleon.colors

echo "Copying desktop theme"

sudo cp -R -v ./desktoptheme/* /usr/share/plasma/desktoptheme/

sudo chmod 755 -R /usr/share/plasma/desktoptheme/kameleon-dark

sudo chmod 755 -R /usr/share/plasma/desktoptheme/kameleon-light

echo "Copying grub theme"

sudo cp -R -v ./grub/* /boot/grub/themes/

sudo chmod 755 -R /boot/grub/themes/kameleon

echo "Copying gtk theme"

sudo cp -R -v ./gtk/* /usr/share/themes/

sudo chmod 755 -R /usr/share/themes/kameleon

echo "Copying icons theme"

sudo cp -R -v ./icons/* /usr/share/icons/

sudo chmod 755 -R /usr/share/icons/kameleon

echo "Copying look and feel theme"

sudo cp -R -v ./look-and-feel/* /usr/share/plasma/look-and-feel/

sudo chmod 755 -R /usr/share/plasma/look-and-feel/org.kde.kameleon.desktop

echo "Copying pixmaps icons"

sudo cp -R -v ./pixmaps/* /usr/share/pixmaps/

sudo chmod 755 /usr/share/pixmaps/desktop-dark.png

sudo chmod 755 /usr/share/pixmaps/desktop-light.png

sudo chmod 755 /usr/share/pixmaps/menu-dark.png

sudo chmod 755 /usr/share/pixmaps/menu-light.png

sudo chmod 755 /usr/share/pixmaps/user-border.png

sudo chmod 755 /usr/share/pixmaps/user-noborder.png

echo "Copying qtcurve theme"

sudo cp -v ./qtcurve/kameleon.qtcurve /usr/share/QtCurve/kameleon.qtcurve

sudo chmod 755 /usr/share/QtCurve/kameleon.qtcurve

echo "Copying sddm theme"

sudo cp -R -v ./sddm/* /usr/share/sddm/themes/

sudo chmod 755 -R /usr/share/sddm/themes/kameleon

echo "Copying wallpapers theme"

sudo cp -R -v ./wallpapers/* /usr/share/wallpapers/

sudo chmod 755 -R /usr/share/wallpapers/kameleon

 
