# Kameleon

## Project

Kameleon is a project to create a full desktop theming to (I know, I know) looking like Windows 10. Nowadays Windows 10 seems to looking like KDE so ...

## Dependencies

Some themes components need packages in order to get the same look as screenshots :

* qtcurve
* virtualkeyboard-plugin

## Install

### User install

Use one of these to install in your user profile folder (all components will be available only for you)

```
make install-user-original 

make install-user-plasma
```

### System install

Use one of these to install in your system folders all the theme (all components will be available for all users)

```
make install-system-original 

make install-system-plasma
```

## Uninstall

Depending on the command you use for install, launch one of these :

```
make uninstall-user-original

make uninstall-user-plasma

make uninstall-system-original

make uninstall-system-plasma
```

## Enhances

This folder contain some artwork relative to all themes, you can find the grub background in xcf format to create your logo and implement it into the grub theme as background, some sample for ROG and Silverstone computer are available.

The plasmoids I use are also available in this folder but most of them can be download directly from KDE system settings.

If you really want to make your KDE looking like a Windows 10 system, you'll find original and alternative lockscreens and wallpapers in this folder.

## Archives

If you want to download only one theme, go into the archives directory and download the one you want.

## Notes

Each theme has a README.md file available regarding of some dependencies (virtualkeyboard, ...).

